import axios from 'axios'
const apiURI = "https://api.sportywave.it";

export async function fetchEvents ({ commit }) {
    try {

        const res = await axios.get(apiURI + '/events')
        await commit('setEvents', res.data)
        return res
    } catch (e) {
        return e.response
    }
}

export async function fetchEventsByUser ({ commit }, id) {
    try {

        const res = await axios.get(apiURI + '/events?partecipantList_in=' + id)
        return res
    } catch (e) {
        return e.response
    }
}

export async function addEvent ({ commit }, event) {
    // Create a new event, and add it to the user profile
    let data = new FormData()
    const body = {}
    body["sport"] = event.sport
    body["description"] = event.description
    body["name"] = event.name
    body["partecipants"] = event.partecipants
    body["address"] = event.address
    body["date"] = event.date
    body["profile"] = event.id
    console.log(body)

    // Check if the file has been uploaded and add it
    if (event.file)
        data.append("files.cover", event.file)

    data.append("data", JSON.stringify(body))
    const res = await axios.post(apiURI + '/events/', data, {
        headers: {
            "Content-Type": "multipart/form-data",
            'Authorization': 'Bearer ' + event.JWT
        }
    })
    return res
}

export async function addEventToUser ({ commit }, event) {
    // Create a new event, and add it to the user profile
    let events = [event.event]

    for (let el in event.user.events) {
        events.push(event.user.events[el])
    }

    const res = await axios.put(apiURI + '/profiles/' + event.user.id, { events: events }, {
        headers: {
            'Authorization': 'Bearer ' + event.JWT
        }
    })
    return res
}