export function isAuthenticated (state) {
    return !!state.user
}

export function StateUser (state) {
    return state.user
}

export function StateProfile (state) {
    return state.profile
}

export function StateJWT (state) {
    return state.JWT
}
