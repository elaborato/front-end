export function setUser(state, username){
    state.user = username
}

export function setProfile(state, profile){
    state.profile = profile
}


export function setJWT(state, JWT){
    state.JWT = JWT
}

export function LogOut(state){
    state.user = null
    state.profile = null
    state.JWT = null
}
