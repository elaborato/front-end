
import axios from 'axios'
const apiURI = "https://api.sportywave.it";

export async function Register ({ commit }, form) {
    try {

        // Register the user
        let body = {
            username: form.username,
            email: form.email,
            password: form.password
        }
        const mainUser = await axios.post(apiURI + '/auth/local/register', form)

        // Create the profile
        body = {
            username: form.username,
            name: form.name,
            bday: form.date,
            surname: form.surname,
            user: mainUser.data.user.id
        }
        return await axios.post(apiURI + '/profiles', body)
    } catch (e) {
        return e.response
    }
}

export async function LogIn ({ commit }, user) {
    // Check the user login and save the email in the store
    let res = await axios.post(apiURI + '/auth/local/', user)
    await commit('setUser', user.identifier)
    await commit('setJWT', res.data.jwt)

    // Fetch the profile and set it in the store
    res = await fetchProfile({ commit }, res.data.user.username)
    await commit('setProfile', res.data[0])

    return res
}

export async function fetchProfile ({ commit }, username) {
    // Fetch the profile from the username
    const res = await axios.get(apiURI + '/profiles/' + username)
    return res

}

export async function editProfile ({ commit }, profile) {
    // Fetch the profile from the id and edit it
    let data = new FormData()
    const body = {}
    if (profile.sports)
        body["Sports"] = profile.sports
    if (profile.description)
        body["description"] = profile.description
    if (profile.socials)
        body["socials"] = profile.socials
    if (profile.file)
        data.append("files.photo", profile.file)

    body["firstLogin"] = false

    data.append("data", JSON.stringify(body))
    const res = await axios.put(apiURI + '/profiles/' + profile.id, data, {
        headers: {
            "Content-Type": "multipart/form-data",
            'Authorization': 'Bearer ' + profile.JWT
        }
    })
    return res

}

export async function LogOut ({ commit }) {
    // Disconnect the user and reset the store
    commit('setUser', null)
    commit('setProfile', null)
    commit('setJWT', null)
}
