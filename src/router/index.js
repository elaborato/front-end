import Vue from 'vue'
import VueRouter from 'vue-router'
//import store from '../store';

import routes from './routes'
Vue.prototype.$apiURI = "https://api.sportywave.it";

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function ({ store }) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  Router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      console.log(store.getters.isAuthenticated)
      if (!store.getters.isAuthenticated) {
        const loginpath = window.location.hash;
        next({ path: '/auth/login', query: { from: loginpath } })
      } else {
        let user = JSON.parse(localStorage.getItem('profile'))
        if (to.matched.some(record => record.meta.is_admin)) {
          if (user.is_admin == 1) {
            next()
          }
          else {
            next({ name: 'userboard' })
          }
        } else {
          next()
        }
      }
    } else if (to.matched.some(record => record.meta.guest)) {
      if (localStorage.getItem('jwt') == null) {
        next()
      }
      else {
        next({ name: 'userboard' })
      }
    } else {
      next()
    }
  })

  return Router
}
