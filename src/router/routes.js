// Import Layouts
import MainLayout from 'layouts/MainLayout.vue'
import AuthLayout from 'layouts/Auth.vue'
import EmptyLayout from 'layouts/Empty.vue'

// Import pages
// 1.0 | Homepage
import Home from 'pages/Index.vue'

// 2.0 | Auth
// 2.1 | Register
import Register from 'pages/auth/Register.vue'
// 2.2 | Login
import Login from 'pages/auth/Login.vue'

// 3.0 | Events
// 3.1 | Events list
import Events from 'pages/Events/Events.vue'
// 3.2 | Add new event [AUTHENTICATED]
import AddEvent from 'pages/Events/AddEvent.vue'
// 3.3 | Participantions list [AUTHENTICATED]
import Participations from 'pages/Events/Participations.vue'

// 4.0 | Profile
// 4.1 | Public user profile
import User from 'pages/profile/User.vue'
// 4.2 | Edit profile [AUTHENTICATED]
import Edit from 'pages/profile/Edit.vue'

const routes = [
  {
    path: '/',
    component: MainLayout,
    children: [
      // 1.0 | Homepage
      {
        path: '',
        component: Home
      },

      // 3.0 | Events
      {
        path: '/events',
        component: EmptyLayout,
        children: [
          // 3.1 | Events list
          { path: '/', component: Events },
          // 3.2 | Add new event [AUTHENTICATED]
          {
            path: 'add', component: AddEvent,
            meta: {
              requiresAuth: true
            }
          },
          // 3.3 | Participantions list [AUTHENTICATED]
          {
            path: 'participations', component: Participations,
            meta: {
              requiresAuth: true
            }
          }
        ]
      },

      // 4.0 | Profile
      {
        path: '/profile',
        component: EmptyLayout,
        children: [
          // 4.1 | Public user profile
          { path: 'user/:username', component: User },
          // 4.2 | Edit profile [AUTHENTICATED]
          {
            path: 'edit', component: Edit,
            meta: {
              requiresAuth: true
            }
          }
        ]
      }
    ]
  },

  // 2.0 | Auth
  {
    path: '/auth/',
    component: AuthLayout,
    children: [
      // 2.1 | Register
      { path: 'register', component: Register },
      // 2.2 | Login
      { path: 'login', component: Login, },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
