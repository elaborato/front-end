
CREATE TABLE IF NOT EXISTS `events` (
  `eventId` mediumint(8) NOT NULL,
  `profileId` mediumint(8) NOT NULL,
  `qrId` mediumint(8) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(80) NOT NULL,
  `address` varchar(30) NOT NULL,
  `sport` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`sport`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `partecipants` (
  `partecipantId` mediumint(8) NOT NULL,
  `profileId` mediumint(8) NOT NULL,
  `eventId` mediumint(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `profiles` (
  `profileId` mediumint(8) NOT NULL,
  `firstLogin` tinyint(1) NOT NULL,
  `name` varchar(30) NOT NULL,
  `surname` varchar(30) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `bday` date DEFAULT NULL,
  `sports` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`sports`)),
  `socials` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`socials`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `q_rcodes` (
  `qrId` mediumint(8) NOT NULL,
  `profileId` mediumint(8) NOT NULL,
  `eventId` mediumint(8) NOT NULL,
  `link` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `roles` (
  `roleId` mediumint(8) NOT NULL,
  `name` varchar(15) NOT NULL,
  `type` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(120) NOT NULL,
  `resetPasswordToken` varchar(30) DEFAULT NULL,
  `confirmationToken` varchar(30) DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL,
  `blocked` tinyint(1) NOT NULL,
  `roleId` mediumint(8) DEFAULT NULL,
  `profileId` mediumint(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `events`
  ADD PRIMARY KEY (`eventId`),
  ADD KEY `ev_profileId` (`profileId`),
  ADD KEY `ev_qrId` (`qrId`);

ALTER TABLE `partecipants`
  ADD PRIMARY KEY (`partecipantId`),
  ADD KEY `pr_eventId` (`eventId`),
  ADD KEY `pr_profileId` (`profileId`);

ALTER TABLE `profiles`
  ADD PRIMARY KEY (`profileId`);

ALTER TABLE `q_rcodes`
  ADD PRIMARY KEY (`qrId`),
  ADD KEY `qr_profileId` (`profileId`),
  ADD KEY `qr_eventId` (`eventId`);

ALTER TABLE `roles`
  ADD PRIMARY KEY (`roleId`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`username`),
  ADD KEY `u_profileId` (`profileId`),
  ADD KEY `u_roleId` (`roleId`);

ALTER TABLE `events`
  MODIFY `eventId` mediumint(8) NOT NULL AUTO_INCREMENT;

ALTER TABLE `partecipants`
  MODIFY `partecipantId` mediumint(8) NOT NULL AUTO_INCREMENT;

ALTER TABLE `profiles`
  MODIFY `profileId` mediumint(8) NOT NULL AUTO_INCREMENT;

ALTER TABLE `q_rcodes`
  MODIFY `qrId` mediumint(8) NOT NULL AUTO_INCREMENT;

ALTER TABLE `roles`
  MODIFY `roleId` mediumint(8) NOT NULL AUTO_INCREMENT;

ALTER TABLE `events`
  ADD CONSTRAINT `ev_profileId` FOREIGN KEY (`profileId`) REFERENCES `profiles` (`profileId`),
  ADD CONSTRAINT `ev_qrId` FOREIGN KEY (`qrId`) REFERENCES `q_rcodes` (`qrId`);

ALTER TABLE `partecipants`
  ADD CONSTRAINT `pr_eventId` FOREIGN KEY (`eventId`) REFERENCES `events` (`eventId`),
  ADD CONSTRAINT `pr_profileId` FOREIGN KEY (`profileId`) REFERENCES `profiles` (`profileId`);

ALTER TABLE `q_rcodes`
  ADD CONSTRAINT `qr_eventId` FOREIGN KEY (`eventId`) REFERENCES `events` (`eventId`),
  ADD CONSTRAINT `qr_profileId` FOREIGN KEY (`profileId`) REFERENCES `profiles` (`profileId`);

ALTER TABLE `users`
  ADD CONSTRAINT `u_profileId` FOREIGN KEY (`profileId`) REFERENCES `profiles` (`profileId`),
  ADD CONSTRAINT `u_roleId` FOREIGN KEY (`roleId`) REFERENCES `roles` (`roleId`);